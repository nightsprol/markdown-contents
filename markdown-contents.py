from argparse import ArgumentParser
from mdparser import MDParser

# Parse the command line arguments
argparser = ArgumentParser()
argparser.add_argument("-i", "--input", help="specify a Markdown file to read from", type=str)
argparser.add_argument("-o", "--output", help="specify a file to write the table of contents to", type=str)
argparser.add_argument(
	"-d", "--detail",
	help="specify the level of detail",
	type=int, default=1, choices=range(1, 7)
)

args = argparser.parse_args()

# Run the parser
try:
	md_parser = MDParser(input_file=args.input, output_file=args.output)
	md_parser.run(args.detail)
except ValueError as ex:
	print(ex)
	exit(1)