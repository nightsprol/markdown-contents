import sys
import util

# Constants
class Const:
	TAB_SIZE = 3
	CODE_TAG = "```"
	HEADER_TAG = '#'

# Parser for markdown files
class MDParser:
	def __init__(self, input_file=None, output_file=None):
		if input_file and not util.verify_file(input_file, "md"):
			raise ValueError("Invalid input file '" + input_file + "'.")

		self.pin = input_file
		self.pout = output_file
		self.links = {}

	def run(self, detail):
		fin, fout = None, None

		# Open files as needed
		if self.pin:
			fin = open(self.pin, "r")
		else:
			fin = sys.stdin

		if self.pout:
			fout = open(self.pout, "w")

		self._run(detail, fin, fout)

		# Clean up resources
		if fin and not fin is sys.stdin:
			fin.close()
		if fout:
			fout.close()

	def _run(self, detail, fin, fout):
		indices = list()			# This will be used to keep track of indices on each level
		in_code_block = False		# We want to keep track of when we enter and exit blocks, eg. code blocks

		for in_line in fin:

			# Check if this is a block
			if in_line.startswith(Const.CODE_TAG):
				in_code_block = not in_code_block
				continue

			# Skip while in a code block
			if in_code_block:
				continue

			h_size, header = self._get_header(in_line)

			# Skip if this is not a header or level of detail is too high
			if not h_size or h_size > detail:
				continue

			while len(indices) < h_size:
				indices.append(1)

			# Otherwise we want to process this header into the list(s)
			out_line = " " * Const.TAB_SIZE * (h_size - 1)

			# Get the title and link
			title, link = self._string_to_link(header)

			# Add the link to the dictionary
			# This allows the program to keep a counter for repeated headers
			if link in self.links:
				self.links[link] += 1
				link += "-" + str(self.links[link])
			else:
				self.links[link] = 0

			out_line += str(indices[h_size - 1]) + ". [" + title + "](#" + link + ")"
			indices[h_size - 1] += 1

			if fout:
				fout.write(out_line + "\n")

			print(out_line)

			while len(indices) > h_size:
				indices.pop()
		# End of _run()

	def _string_to_link(self, string):
		_string = string.replace('-', ' ').replace('\n', '')
		md_link = ""
		had_whitespace = False

		for c in _string:
			if c.isspace():
				if had_whitespace:
					continue
				else:
					had_whitespace = True
				md_link += '-'
			elif not c.isalpha() and not c.isdigit():
				continue
			else:
				md_link += c
				had_whitespace = False

		return string.replace('\n', ''), md_link.lower()

	def _get_header(self, header):
		count, index = 0, 0

		for c in header:
			if c is not Const.HEADER_TAG and not c.isspace():
				break
			if c is Const.HEADER_TAG:
				count += 1
			index += 1

		if not count:
			return 0, header

		return count, header[index:]