import os

def verify_file(filepath, extension):
	if not os.path.isfile(filepath):
		return False

	file_ext = get_extension(filepath)

	if not file_ext == extension:
		return False

	return True

# Retrieve the extension of a file
def get_extension(filepath):
	for i in range(len(filepath) - 1, 0, -1):
		if filepath[i] == '.':
			return filepath[i+1:]
	return filepath