# markdown-contents

This is a command-line utility that will auto parse a markdown file and generate a table of contents from the headers. Below is an example of it being run on this readme. The program is meant to be as simple to use as possible; there is no need for all sorts of bloated features. It simply parses a Markdown file and gives you the corresponding table of contents.

# Table of Contents

1. [markdown-contents](#markdown-contents)
2. [Table of Contents](#table-of-contents)
3. [Usage](#usage)
   1. [Downloading](#downloading)
   2. [Running](#running)
   3. [Example Run](#example-run)
   4. [Advanced Use](#advanced-use)
4. [Contributing](#contributing)
5. [Authors](#authors)

# Usage

## Downloading

To use this tool, first clone the repository to download the files, then run the program with `-h` to see the usage.

```sh
$ git clone git@gitlab.com:nightsprol/markdown-contents.git
$ cd markdown-contents
$ python markdown-contents -h
```

## Running

To run the basic program easily, all you need to do is specify an input file.

```sh
$ python markdown-contents -i <in_file>
```

The above will read from `in_file` and print the table of contents to your console. You can also specify an output file with `-o <output_file>`. If you do not specify an input or output, it will default to stdin or stdout respectively. This is to make it possible to use redirection and piping if you prefer that.

As you would expect, it will work for any combination of symbols in the headers and will correctly strip them out to generate the appropriate link. It also supports multiple headers with the same name. You simply need to tell it what Markdown file you want to generate a table of contents for!

## Example Run

As an example you could run the program on this README.md file. First make sure you are in the `markdown-contents` directory, then perform the following command:

```sh
$ python markdown-contents -i README.md
```

The above will generate a table of contents from the top-level headers and print it to your console. You should see the following:

```md
1. [markdown-contents](#markdown-contents)
2. [Table of Contents](#table-of-contents)
3. [Usage](#usage)
4. [Contributing](#contributing)
5. [Authors](#authors)
```

## Advanced Use

You can also specify additional flags. Besides `-i` and `-o` for input and output files, you can use the `-d` or `--detail` flag. This must have an argument in the range [1, 6] and will determine the level of detail output by the program.

The example mentioned previously will output a table of contents with only the top-level headers, because the default level of detail is 1. If you add the argument `-d 2` it will also list the first sublevel of headers, which will include things like this section (Advanced Use).

```sh
$ python markdown-contents -i README.md -d 2
```

The output is the following, which is the same as the table of contents used in this readme:

```md
1. [markdown-contents](#markdown-contents)
2. [Table of Contents](#table-of-contents)
3. [Usage](#usage)
   1. [Downloading](#downloading)
   2. [Running](#running)
   3. [Example Run](#example-run)
   4. [Advanced Use](#advanced-use)
4. [Contributing](#contributing)
5. [Authors](#authors)
```

Lastly, because the program takes input from `stdin` and outputs to `stdout` by default, you can perform more complicated commands in your terminal that involve redirection or piping, such as the following:

```sh
$ python markdown-contents -d 2 < README.md
```

The above command runs the program with the level of detail set to 2 and redirects the `stdin` to take input from this readme file. The output will be the same as the last example.

# Contributing

You can contribute by cloning the master branch and creating your own separate development branch.

```sh
$ git clone git@gitlab.com:nightsprol/markdown-contents.git
$ cd markdown-contents
$ git branch <new-branch>
```

# Authors

 - Nate Wood